using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [Header("PLAYER ATTRIBUTES--------------------")]
    [Space(5)]
    [SerializeField] float _speed = 20.0f;
    [SerializeField] int _lives = 4;
    [SerializeField] int _souls = 0;

    [Header("PLAYER BOUNDRIES--------------------")]
    [Space(5)]
    [SerializeField] float _xClampMin = -18f;
    [SerializeField] float _xClampMax = 18f;
    [SerializeField] float _zClampMin = -10f;
    [SerializeField] float _zClampMax = 30f;

    [Header("FIREBALL ATTRIBUTES--------------------")]
    [Space(5)]
    [SerializeField] GameObject _fireballPrefab;
    [SerializeField] float _fireRate = 0.5f;
    private float _canFireTime = 0.0f;

    [Header("PARTICLE EFFECTS--------------------")]
    [Space(5)]
    [SerializeField] GameObject _playerDamageParticle;
    [SerializeField] float _effectOffset = 5.75f;

    [Header("AUDIO CLIPS--------------------")]
    [Space(5)]
    [SerializeField] AudioClip _playerDamageSound;
    [SerializeField] AudioSource _audioSource;
    
    [Header("UNITY COMPONENTS--------------------")]
    [Space(5)]
    [SerializeField] SpawnManager _spawnManager;
    [SerializeField] UIManager _uIManager;
    


    Vector3 _spawnPointOffset = new Vector3(-.92f, 1.41f, -1.246f);


    private void Start()
    {
       
        transform.position = new Vector3(0, 0, 0);

        _spawnManager = GameObject.Find("Spawn_Manager").GetComponent<SpawnManager>();

        _uIManager = GameObject.Find("Main Canvas").GetComponent<UIManager>();

        _audioSource = GetComponent<AudioSource>();

        if (_spawnManager == null)
        {
            Debug.LogError("The Spawn Manager is NULL");
        }

        if (_uIManager == null)
        {
            Debug.LogError("UI Manager is NULL");
        }

        if(_audioSource == null)
        {
            Debug.LogError("The Audio Source on the player is NULL");
        }
        else
        {
            _audioSource.clip = _playerDamageSound;
        }
               
    }
      
    void Update()
    {
        CalculateMovement();

        if (Input.GetKeyDown(KeyCode.Space) && Time.time > _canFireTime)
        {
            Shoot();
        }
    }

    private void CalculateMovement()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        Vector3 direction = new Vector3(horizontalInput, 0, verticalInput);

        transform.Translate(direction * _speed * Time.deltaTime);

        // Vertical Clamp
        transform.position = new Vector3(transform.position.x, 0, Mathf.Clamp(transform.position.z, _zClampMin, _zClampMax));
        
        //Horizontal Clamp
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, _xClampMin, _xClampMax), 0, transform.position.z);
       
    }

    private void Shoot()
    {
        _canFireTime = Time.time + _fireRate;
        Instantiate(_fireballPrefab, transform.position + _spawnPointOffset, Quaternion.identity);
    }

    public void Damage()
    {
        _lives--;

        PlayerDamageParticle();

        _audioSource.Play();
       
        _uIManager.UpdateLivesDisplay(_lives);

        if (_lives <= 0)
        {
            PlayerDamageParticle();
            Destroy(this.gameObject);
            _spawnManager.OnPlayerDeath();
            _uIManager.GameOverDisplay();
        }
    }
   
    public void AddSoul(int soulAmount)
    {
        _souls += soulAmount;
        _uIManager.UpdateSouls(_souls);
    }

    public void PlayerDamageParticle()
    {
        Instantiate(_playerDamageParticle,
            new Vector3(transform.position.x, transform.position.y + _effectOffset, transform.position.z),
            Quaternion.identity);
     
    }
    

}
