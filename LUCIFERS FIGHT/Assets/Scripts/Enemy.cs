using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [Header("ENEMY ATTRIBUTES--------------------")]
    [Space(5)]
    [SerializeField] GameObject _enemyGameOject;
    [SerializeField] CapsuleCollider _enemyCapsuleCollider;
    [SerializeField] float _speed = 4.0f;
    [SerializeField] int _soulAmount = 1;
    [SerializeField] float _killTimeDelay = 0.75f;

    [Header("ENEMY BOUNDRIES--------------------")]
    [Space(5)]
    [SerializeField] float _topOfScreen = 60.0f;
    [SerializeField] float _bottomOfScreen = -45.0f;
    [SerializeField] float _xSideMin = -18f;
    [SerializeField] float _xSideMax = 18f;

    [Header("PARTICLE EFFECTS--------------------")]
    [Space(5)]
    [SerializeField] GameObject _enemyDamageParticle;

    [Header("AUDIO CLIPS--------------------")]
    [Space(5)]
    [SerializeField] AudioClip _enemyDamageSound;
    [SerializeField] AudioSource _audioSource;

    [Header("UNITY COMPONENTS--------------------")]
    [Space(5)]
    [SerializeField] Player _player;

    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Player").GetComponent<Player>();
        _enemyCapsuleCollider = GetComponent<CapsuleCollider>();

        if (_audioSource == null)
        {
            Debug.LogError("The Audio Source on the Enemy is NULL");
        }
        else
        {
            _audioSource.clip = _enemyDamageSound;
        }

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * _speed * Time.deltaTime);

        if(transform.position.z > _topOfScreen)
        {
            transform.position = new Vector3(Random.Range(_xSideMin, _xSideMax), 0, _bottomOfScreen);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Player player = other.transform.GetComponent<Player>();

            // Have angels fire on the player and take a life
            if (player != null)
            {
                player.Damage();

                _audioSource.Play();

                EnemyDamageParticle();

                _enemyGameOject.SetActive(false);
                _enemyCapsuleCollider.enabled = false;
            }
        
            Destroy(this.gameObject, _killTimeDelay);
        }

        if(other.CompareTag("Fireball"))
        {
            if(_player != null)
            {
                _player.AddSoul(_soulAmount);

                _audioSource.Play();

                EnemyDamageParticle();

                _enemyGameOject.SetActive(false);
                _enemyCapsuleCollider.enabled = false;
            }

            Destroy(this.gameObject, _killTimeDelay);
        }

    }

    public void EnemyDamageParticle()
    {
        Instantiate(_enemyDamageParticle, transform.position, Quaternion.identity);
    }

}
