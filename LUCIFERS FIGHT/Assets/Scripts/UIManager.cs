using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI _soulText;
    [SerializeField] GameObject _gameOverText;
    [SerializeField] GameObject _gameInstructions;
    [SerializeField] Button _menuButton;

    [Header("PLAYER LIVES Immages--------------------")]
    [Space(5)]
    [SerializeField] Image _lifeOne;
    [SerializeField] Image _lifeTwo;
    [SerializeField] Image _lifeThree;
    [SerializeField] Image _lifeFour;
    

    private void Start()
    {
        _soulText.text = "SOULS: " + 0;
    }

    public void UpdateSouls(int soulScore)
    {
        _soulText.text = "SOULS: " + soulScore;
    }

    public void UpdateLivesDisplay(int currentLives)
    {

        if (currentLives < 1)
        {
            _lifeOne.enabled = false;
        }
        else if (currentLives < 2)
        {
            _lifeTwo.enabled = false;
        }
        else if (currentLives < 3)
        {
            _lifeThree.enabled = false;
        }
        else if (currentLives <= 4)
        {
            _lifeFour.enabled = false;
        }
    }

    public void GameOverDisplay()
    {
        _gameOverText.gameObject.SetActive(true);
    }

    public void PauseGame()
    {
        _menuButton.gameObject.SetActive(true);
        _gameInstructions.gameObject.SetActive(true);
                    
    }

    
}
