using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [Header("SPAWN MANAGER ATTRIBUTES--------------------")]
    [Space(5)]
    [SerializeField] float _spawnTime = 5.0f;
    [SerializeField] GameObject _enemyPrefab;
    [SerializeField] GameObject _enemyContainer;

    private bool _stopSpawning = false;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnRoutine());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    IEnumerator SpawnRoutine()
    {
        while(_stopSpawning == false)
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-18.0f, 18.0f), 0, -40.0f);
            
            GameObject newEnemy = Instantiate(_enemyPrefab, spawnPosition, Quaternion.identity);
            newEnemy.transform.parent = _enemyContainer.transform;

            yield return new WaitForSeconds(_spawnTime);

        }
    }

    public void OnPlayerDeath()
    {
        _stopSpawning = true;
    }
}
