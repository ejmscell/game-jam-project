using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour
{
    [Header("FIREBALL ATTRIBUTES--------------------")]
    [Space(5)]
    [SerializeField] float _speed = 5.0f;
    [SerializeField] float _destroyPositionZ = -45.0f;

    [Header("AUDIO CLIPS--------------------")]
    [Space(5)]
    [SerializeField] AudioClip _shotSound;
    [SerializeField] AudioSource _audioSource;

    void Update()
    {
        CalculateMovment();
    }

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();

        if (_audioSource == null)
        {
            Debug.LogError("The Audio Source on the Fireball is NULL");
        }
        else
        {
            _audioSource.clip = _shotSound;
        }

        _audioSource.Play();
    }

    private void CalculateMovment()
    {
        transform.Translate(0, 0, -5 * _speed * Time.deltaTime);

        //if laser position is less than -45 on the z, then destroy game object
        if (transform.position.z <= _destroyPositionZ)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
        }

        
    }

    
}
